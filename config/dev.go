package config

import "os"

//Dev sets development environment variables and returns config
func Dev() *Config {
	//API environment variables
	os.Setenv("API_PORT", "8080")
	//DB environment variables
	os.Setenv("DB_HOST", "localhost")
	os.Setenv("DB_PORT", "5432")
	os.Setenv("DB_USER", "test")
	os.Setenv("DB_PASSWORD", "test")
	os.Setenv("DB_NAME", "test")
	return get()
}
