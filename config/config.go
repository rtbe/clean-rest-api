package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
)

var (
	once   sync.Once
	config *Config
)

//Config holds environment variables
type Config struct {
	APIPort    string
	PgHost     string
	PgPort     string
	PgUser     string
	PgPassword string
	PgDBName   string
}

//Get constructs config from environment variables
func get() *Config {
	//Read ENV`s only once
	once.Do(func() {
		config = &Config{
			APIPort:    os.Getenv("API_PORT"),
			PgHost:     os.Getenv("DB_HOST"),
			PgPort:     os.Getenv("DB_PORT"),
			PgUser:     os.Getenv("DB_USER"),
			PgPassword: os.Getenv("DB_PASSWORD"),
			PgDBName:   os.Getenv("DB_NAME"),
		}
		configJSON, err := json.MarshalIndent(config, "", " ")
		if err != nil {
			log.Fatal(err)
		}

		//Print configuration as JSON into terminal
		fmt.Println("Configuration:", string(configJSON))
	})
	return config
}
