package user

import (
	"example.com/clean-rest-api/entity"
)

//Repository interface
type Repository interface {
	Get(string) (*entity.User, error)
	Create(*entity.User) (*entity.User, error)
	Delete(string) error
	List() ([]entity.User, error)
}

//UseCase interface
type UseCase interface {
	GetUser(string) (*entity.User, error)
	CreateUser(string, string, string, string, string) (*entity.User, error)
	DeleteUser(string) error
	ListUsers() ([]entity.User, error)
}
