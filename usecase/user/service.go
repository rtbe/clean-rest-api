package user

import (
	"example.com/clean-rest-api/entity"
)

//Service is an intermidiate layer between User entity and User DB layer (repository)
type Service struct {
	repo Repository
}

//NewUserService creates new User entity service
func NewUserService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

//CreateUser creates new User
func (s *Service) CreateUser(firstName, lastName, password, email, role string) (*entity.User, error) {
	u, err := entity.NewUser(firstName, lastName, password, email, role)
	if err != nil {
		return u, err
	}

	return s.repo.Create(u)
}

//GetUser gets User
func (s *Service) GetUser(id string) (*entity.User, error) {
	return s.repo.Get(id)
}

//DeleteUser deletes User
func (s *Service) DeleteUser(id string) error {
	return s.repo.Delete(id)
}

//ListUsers lists Users
func (s *Service) ListUsers() ([]entity.User, error) {
	return s.repo.List()
}
