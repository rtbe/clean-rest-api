package model

import "time"

type UserJSON struct {
	UserID    string `json:"user_id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}
type UserDB struct {
	UserID       string    `db:"user_id"`
	FirstName    string    `db:"first_name"`
	LastName     string    `db:"last_name"`
	PasswordHash string    `db:"password_hash"`
	Email        string    `db:"email"`
	Role         string    `db:"role"`
	DateCreated  time.Time `db:"date_created"`
	DateUpdated  time.Time `db:"date_updated"`
}
