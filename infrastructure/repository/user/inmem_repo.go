package repo

import (
	"errors"
	"fmt"
	"sync"

	"example.com/clean-rest-api/entity"
)

//UserInMemRepo is an abstraction layer that manages user entities inside basic in-memory store (map+RWMutex)
type UserInMemRepo struct {
	store map[string]entity.User
	sync.RWMutex
}

//NewUserInMemRepo creates new in-memory repository for User entity
func NewUserInMemRepo() *UserInMemRepo {
	m := make(map[string]entity.User)
	return &UserInMemRepo{
		store: m,
	}
}

//Get gets user from in-memory store
func (r *UserInMemRepo) Get(id string) (*entity.User, error) {
	u, err := r.store[id]
	if !err {
		return nil, errors.New("There are no user with this id")
	}
	return &u, nil
}

//Create creates new user in in-memory store
func (r *UserInMemRepo) Create(u *entity.User) (*entity.User, error) {
	r.Lock()
	defer r.Unlock()

	r.store[u.UserID] = *u
	return u, nil
}

//Delete deletes user from in-memory store
func (r *UserInMemRepo) Delete(id string) error {
	r.Lock()
	defer r.Unlock()

	_, err := r.store[id]
	if !err {
		return fmt.Errorf("User with id:%v was not found", id)
	}
	delete(r.store, id)
	return nil
}

//List lists all users from in-memory store
func (r *UserInMemRepo) List() ([]entity.User, error) {
	users := make([]entity.User, 0)
	for _, user := range r.store {
		users = append(users, user)
	}
	if len(users) == 0 {
		return nil, errors.New("There are no users")
	}
	return users, nil
}
