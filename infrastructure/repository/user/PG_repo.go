package repo

import (
	"errors"
	"fmt"

	"example.com/clean-rest-api/entity"
	"github.com/jmoiron/sqlx"
)

//UserPGRepo is an abstraction layer that manages user entities inside PostgreSQL DB
type UserPGRepo struct {
	db *sqlx.DB
}

//NewUserPGRepo creates new PostgreSQL repository for User entity
func NewUserPGRepo(db *sqlx.DB) *UserPGRepo {
	return &UserPGRepo{
		db: db,
	}
}

//Get gets user from PostgreSQL DB
func (r *UserPGRepo) Get(id string) (*entity.User, error) {
	var u entity.User
	err := r.db.Get(&u, `SELECT FIRST_NAME,LAST_NAME,EMAIL,DATE_CREATED,DATE_UPDATED FROM USERS WHERE USER_ID=$1`, id)
	return &u, err
}

//Create creates new user in PostgreSQL DB
func (r *UserPGRepo) Create(u *entity.User) (*entity.User, error) {
	_, err := r.db.NamedExec(`
		INSERT INTO USERS (USER_ID, FIRST_NAME, LAST_NAME, PASSWORD_HASH, EMAIL, DATE_CREATED, DATE_UPDATED) 
		VALUES(:userID,:firstName,:lastName,:password,:email,:dateCreated,:dateUpdated)`,
		map[string]interface{}{
			"userID":      u.UserID,
			"firstName":   u.FirstName,
			"lastName":    u.LastName,
			"password":    u.PasswordHash,
			"email":       u.Email,
			"role":        u.Role,
			"dateCreated": u.DateCreated,
			"dateUpdated": u.DateCreated,
		})
	if err != nil {
		return u, err
	}
	return u, nil
}

//Delete deletes user from PostgreSQL DB
func (r *UserPGRepo) Delete(id string) error {
	res, err := r.db.Exec(`DELETE FROM USERS WHERE USER_ID=$1`, id)
	if err != nil {
		return err
	}
	if i, _ := res.RowsAffected(); i == 0 {
		return fmt.Errorf("User with id:%v was not found", id)
	}
	return nil
}

//List lists all users from PostgreSQL DB
func (r *UserPGRepo) List() ([]entity.User, error) {
	users := make([]entity.User, 0)

	rows, err := r.db.Queryx(`
		SELECT FIRST_NAME, LAST_NAME, EMAIL, DATE_CREATED, DATE_UPDATED FROM USERS
	`)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var user entity.User
		err := rows.StructScan(&user)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	if len(users) == 0 {
		return nil, errors.New("There are no users")
	}

	return users, nil
}
