module example.com/clean-rest-api

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/google/uuid v1.1.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.8.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	google.golang.org/appengine v1.6.7 // indirect
)
