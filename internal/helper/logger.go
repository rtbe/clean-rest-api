package logger

import (
	"log"
	"time"
)

//ASCI colour codes
//https://golangbyexample.com/print-output-text-color-console/
var (
	colorReset string = "\033[0m"

	colorRed    string = "\033[31m"
	colorGreen  string = "\033[32m"
	colorYellow string = "\033[33m"
	colorBlue   string = "\033[34m"
	colorPurple string = "\033[35m"
	colorCyan   string = "\033[36m"
	colorWhite  string = "\033[37m"
)

//Logger is a wrapper around default log.Logger to give it more functionality
type Logger struct {
	*log.Logger
}

//New returns a new logger
func New(l *log.Logger) *Logger {
	return &Logger{l}
}

//RequestLog logs coloured info about request
//RequestLog uses closure to track request execution time
func (l *Logger) RequestLog() func(string, string, string, error) {
	start := time.Now()
	return func(handler, method, url string, err error) {
		if err != nil {
			l.Printf("%s [ERROR] %s HandlerFunc:%s Method:%s URL:%s ExecTime:%s %s\n", colorRed, err, handler, method, url, time.Since(start), colorReset)
			return
		}
		l.Printf("%s [OK] HandlerFunc:%s Method:%s URL:%s ExecTime:%s %s\n", colorGreen, handler, method, url, time.Since(start), colorReset)
	}
}
