package entity

import (
	"errors"
	"time"

	"github.com/google/uuid"
)

//Product defines model for product
type Product struct {
	ProductID   string    `db:"product_id" json:"product_id"`
	Name        string    `db:"name" json:"name"`
	Description string    `db:"description" json:"descripton"`
	Cost        int       `db:"cost" json:"cost"`
	Quantity    int       `db:"quantity" json:"quantity"`
	DateCreated time.Time `db:"date_created" json:"date_created"`
	DateUpdated time.Time `db:"date_updated" json:"date_updated"`
}

//NewProduct creates a new Product
func NewProduct(name, description string, cost, quantity int) (*Product, error) {
	p := &Product{
		ProductID:   uuid.New().String(),
		Name:        name,
		Description: description,
		Cost:        cost,
		Quantity:    quantity,
		DateCreated: time.Now(),
	}
	err := p.validate()
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (p *Product) validate() error {
	if p.Name == "" || p.Description == "" {
		return errors.New("")
	}
	return nil
}
