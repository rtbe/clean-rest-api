package entity

import (
	"errors"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

var (
	errInvalidUser = errors.New("User has empty fields")
)

//User defines model for User
//This type couples json fields with db fields so you can decouple it later by replacing it with separate userDB struct
type User struct {
	UserID       string    `db:"user_id" json:"user_id,omitempty"`
	FirstName    string    `db:"first_name" json:"first_name"`
	LastName     string    `db:"last_name" json:"last_name"`
	PasswordHash string    `db:"password_hash" json:"-"`
	Email        string    `db:"email" json:"email"`
	Role         string    `db:"role" json:"-"`
	DateCreated  time.Time `db:"date_created" json:"date_created"`
	DateUpdated  time.Time `db:"date_updated" json:"date_updated,omitempty"`
}

//NewUser creates a new user
func NewUser(firstName, lastName, password, email, role string) (*User, error) {
	hash, err := generatePassword(password)
	if err != nil {
		return nil, err
	}
	u := &User{
		UserID:       uuid.New().String(),
		FirstName:    firstName,
		LastName:     lastName,
		PasswordHash: hash,
		Email:        email,
		Role:         role,
		DateCreated:  time.Now(),
	}

	err = u.validate()
	if err != nil {
		return nil, errInvalidUser
	}
	return u, nil
}

//Validate user struct fields
func (u *User) validate() error {
	if u.FirstName == "" || u.LastName == "" || u.PasswordHash == "" || u.Email == "" {
		return errInvalidUser
	}
	return nil
}

func generatePassword(s string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

//ValidatePassword validates password
func (u *User) ValidatePassword(p string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(p))
}
