package handler

import (
	"encoding/json"
	"net/http"

	logger "example.com/clean-rest-api/internal/helper"
	"github.com/go-chi/chi"
)

//Handler is a handler with nested logger and router
type Handler struct {
	Logger *logger.Logger
	Router *chi.Mux
}

//New creates new Handler
func New(l *logger.Logger, r *chi.Mux) *Handler {
	return &Handler{
		Logger: l,
		Router: r,
	}
}

//RespondWithJSON is a helper for handling json responses
func respondWithJSON(message string, data interface{}, statusCode int, w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	jsonMap := make(map[string]interface{})
	jsonMap[message] = data

	json.NewEncoder(w).Encode(jsonMap)
}

//RespondWithError is a helper for handling json responses with errors
func respondWithError(data interface{}, statusCode int, w http.ResponseWriter) {
	respondWithJSON("error", data, statusCode, w)
}
