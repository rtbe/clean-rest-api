package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"example.com/clean-rest-api/api/middleware"
	"example.com/clean-rest-api/entity"
	logger "example.com/clean-rest-api/internal/helper"
	"example.com/clean-rest-api/usecase/user"
	"github.com/go-chi/chi"
)

var (
	errContentType = errors.New("Wrong content-type header")
	errListUsers   = errors.New("Error reading users")
	errGetUser     = errors.New("Error getting user")
	errCreateUser  = errors.New("Error adding user")
	errDeleteUser  = errors.New("Error removing user")
)

//InitUserRoutes initializes /user subrouter
func (h *Handler) InitUserRoutes(service user.UseCase) {
	h.Router.Route("/user", func(r chi.Router) {
		r.Use(middleware.Cors)
		r.Get("/", listUsers(h.Logger, service))
		r.Post("/", createUser(h.Logger, service))
		r.Get("/{id}", getUser(h.Logger, service))
		r.Delete("/{id}", deleteUser(h.Logger, service))
	})
}

func listUsers(l *logger.Logger, service user.UseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		closuredLog := l.RequestLog()

		usersSl, err := service.ListUsers()
		if err != nil {
			respondWithError(errListUsers.Error(), http.StatusInternalServerError, w)
			closuredLog("listUsers", r.Method, r.URL.String(), err)
			return
		}

		respondWithJSON("data", usersSl, http.StatusOK, w)
		closuredLog("listUsers", r.Method, r.URL.String(), nil)
	}
}

func getUser(l *logger.Logger, service user.UseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		closuredLog := l.RequestLog()

		id := chi.URLParam(r, "id")
		u, err := service.GetUser(id)
		if err != nil {
			respondWithError(errGetUser.Error(), http.StatusBadRequest, w)
			closuredLog("getUser", r.Method, r.URL.String(), err)
			return
		}
		respUser := entity.User{
			FirstName:    u.FirstName,
			LastName:     u.LastName,
			PasswordHash: u.PasswordHash,
			Email:        u.Email,
			DateCreated:  u.DateUpdated,
			DateUpdated:  u.DateUpdated,
		}

		respondWithJSON("data", respUser, http.StatusOK, w)
		closuredLog("getUser", r.Method, r.URL.String(), nil)
	}
}

func createUser(l *logger.Logger, service user.UseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		closuredLog := l.RequestLog()
		if str := r.Header.Get("Content-Type"); str != "application/json" {
			respondWithError(errContentType.Error(), http.StatusBadRequest, w)
			closuredLog("createUser", r.Method, r.URL.String(), errContentType)
			return
		}

		var decodedUser entity.User
		err := json.NewDecoder(r.Body).Decode(&decodedUser)
		if err != nil {
			respondWithError(errCreateUser.Error(), http.StatusInternalServerError, w)
			closuredLog("createUser", r.Method, r.URL.String(), err)
			return
		}
		u, err := service.CreateUser(decodedUser.FirstName, decodedUser.LastName, decodedUser.PasswordHash, decodedUser.Email, decodedUser.Role)
		if err != nil {
			respondWithError(errCreateUser.Error(), http.StatusInternalServerError, w)
			closuredLog("createUser", r.Method, r.URL.String(), err)
			return
		}

		respondWithJSON("data", u, http.StatusCreated, w)
		closuredLog("createUser", r.Method, r.URL.String(), nil)
	}
}

func deleteUser(l *logger.Logger, service user.UseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		closuredLog := l.RequestLog()

		id := chi.URLParam(r, "id")
		err := service.DeleteUser(id)
		if err != nil {
			respondWithError(errDeleteUser.Error(), http.StatusInternalServerError, w)
			closuredLog("deleteUser", r.Method, r.URL.String(), err)
			return
		}

		respondWithJSON("data", fmt.Sprint("User was successfully deleted"), http.StatusOK, w)
		closuredLog("deleteUser", r.Method, r.URL.String(), nil)
	}
}
