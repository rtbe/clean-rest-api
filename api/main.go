package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"example.com/clean-rest-api/api/handler"
	"example.com/clean-rest-api/config"
	repo "example.com/clean-rest-api/infrastructure/repository/user"
	logger "example.com/clean-rest-api/internal/helper"
	"example.com/clean-rest-api/usecase/user"
	"github.com/go-chi/chi"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func main() {

	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	cfg := config.Dev()
	logger := logger.New(log.New(os.Stdout, "REST API: ", log.LstdFlags))
	router := chi.NewRouter()
	handler := handler.New(logger, router)

	dbConnStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", cfg.PgHost, cfg.PgPort, cfg.PgDBName, cfg.PgPassword, cfg.PgDBName)

	postgresDB, err := sqlx.Connect("postgres", dbConnStr)
	if err != nil {
		return err
	}
	defer postgresDB.Close()

	//In-memory userRepo implementation
	//userRepo := repo.NewUserInMemRepo()

	userRepo := repo.NewUserPGRepo(postgresDB)

	userService := user.NewUserService(userRepo)

	handler.InitUserRoutes(userService)

	s := http.Server{
		Addr:         ":" + cfg.APIPort,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      handler.Router,
	}

	return s.ListenAndServe()
}
